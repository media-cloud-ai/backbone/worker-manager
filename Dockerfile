FROM rust:1.76.0 as builder

ADD . /src
WORKDIR /src

RUN apt-get update && \
    apt install -y curl && \
    curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl &&\
    chmod +x ./kubectl &&\
    mv ./kubectl /usr/bin/kubectl &&\
    cargo build --verbose --release && \
    cargo install --path .

FROM debian:bookworm
COPY --from=builder /usr/local/cargo/bin/rs_worker_manager /usr/bin
COPY --from=builder /usr/bin/kubectl /usr/bin

RUN apt update && apt install -y openssl ca-certificates
ENV AMQP_QUEUE=job_worker_manager
CMD rs_worker_manager
