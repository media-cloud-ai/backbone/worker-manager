mod error;
mod execution_environment;
mod message;

use mcai_worker_sdk::{default_rust_mcai_worker_description, job::JobResult, prelude::*};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

default_rust_mcai_worker_description!();

#[derive(Clone, Debug, Deserialize, JsonSchema, Serialize)]
pub struct GPU {
  model: Option<String>,
}

#[derive(Clone, Debug, Deserialize, JsonSchema, Serialize)]
#[serde(rename_all = "snake_case")]
pub enum ExecutionTarget {
  Docker,
  Kubernetes,
}

#[derive(Clone, Debug, Deserialize, JsonSchema, Serialize)]
pub struct Sidecar {
  registry: Option<String>,
  name: String,
  version: Option<String>,
  cpu: Option<String>,
  ram: Option<String>,
  gpu: Option<GPU>,
  scale: Option<usize>,
}

#[derive(Clone, Debug, Deserialize, JsonSchema, Serialize)]
pub struct WorkerConfiguration {
  exec: ExecutionTarget,
  registry: Option<String>,
  name: String,
  version: Option<String>,
  cpu: Option<String>,
  ram: Option<String>,
  gpu: Option<GPU>,
  scale: Option<usize>,
  sidecars: Option<Vec<Sidecar>>,
}

#[derive(Clone, Debug, Deserialize, JsonSchema, Serialize)]
pub struct Ports {
  #[serde(default)]
  inputs: Vec<u64>,
  #[serde(default)]
  outputs: Vec<u64>,
}

#[derive(Clone, Debug, Deserialize, JsonSchema, Serialize)]
#[serde(rename_all = "snake_case")]
pub enum Action {
  Create,
  Delete,
}

#[derive(Clone, Debug, Deserialize, JsonSchema, Serialize)]
pub struct WorkerParameters {
  namespace: Option<String>,
  direct_messaging_queue_name: Option<String>,
  action: Action,
  worker: WorkerConfiguration,
  ports: Ports,
}

#[derive(Debug, Default)]
struct WorkerManagerEvent {}

impl McaiWorker<WorkerParameters, RustMcaiWorkerDescription> for WorkerManagerEvent {
  fn process(
    &self,
    channel: Option<McaiChannel>,
    parameters: WorkerParameters,
    job_result: JobResult,
  ) -> Result<JobResult> {
    message::process(channel, parameters, job_result)
  }
}

fn main() {
  let message_event = WorkerManagerEvent::default();
  start_worker(message_event);
}
