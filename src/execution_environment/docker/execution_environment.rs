use super::generator::*;
use crate::{
  error::{
    DockerAction::{
      ConnectClient, CreateContainer, GetContainer, InspectContainer, PullImage, RemoveContainer,
      RunContainer, StopContainer,
    },
    Error, Result,
  },
  execution_environment::{
    tools::{create_image_name, get_worker_name},
    ExecutionEnvironment,
  },
  WorkerParameters,
};
use mcai_docker::{container, image};
use mcai_worker_sdk::prelude::*;

pub struct DockerExecutionEnvironment {}

#[async_trait::async_trait]
impl ExecutionEnvironment for DockerExecutionEnvironment {
  async fn create(
    &self,
    parameters: &WorkerParameters,
    job_result: JobResult,
  ) -> Result<JobResult> {
    let generator = DockerGenerator::new(parameters, job_result.get_job_id())
      .map_err(|e| Error::ConfigurationParsing(Box::new(e)))?;

    let docker = mcai_docker::Docker::connect_with_socket_defaults()
      .map_err(|e| Error::Docker(ConnectClient, e.to_string()))?;

    let image = create_image_name(
      parameters.worker.registry.clone(),
      parameters.worker.name.clone(),
      parameters.worker.version.clone(),
    );

    let builder = image::Image::new(&image)
      .with_credentials(generator.credentials())
      .build(&docker)
      .await
      .map_err(|e| Error::Docker(PullImage, e.to_string()))?;

    let envs: Vec<(&str, &(dyn ToString + Send + Sync))> = generator.env();

    let container = builder
      .with_name(&generator.worker_name())
      .with_network("mediacloudai_global")
      .with_envs(envs.as_slice())
      .with_env(
        "AMQP_QUEUE",
        &format!("job_live_{}", generator.worker_name()),
      )
      .with_env(
        "DIRECT_MESSAGING_IDENTIFIER",
        &parameters.direct_messaging_queue_name.as_ref().unwrap(),
      )
      .with_volume_bindings(&generator.volumes())
      .with_port_bindings(&generator.port_bindings())
      .build(&docker)
      .await
      .map_err(|e| Error::Docker(CreateContainer, e.to_string()))?;

    container
      .run(&docker, None)
      .await
      .map_err(|e| Error::Docker(RunContainer, e.to_string()))?;

    let container_inspect = container
      .inspect(&docker, false)
      .await
      .map_err(|e| Error::Docker(InspectContainer, e.to_string()))?;

    debug!("container_id: {:?}", container.id);

    let mut output_parameters: Vec<Parameter> = vec![
      Parameter {
        id: "direct_messaging_queue_name".to_string(),
        kind: "string".to_string(),
        value: Some(serde_json::Value::String(
          parameters.direct_messaging_queue_name.clone().unwrap(),
        )),
        store: None,
        default: None,
      },
      Parameter {
        id: "host_ip".to_string(),
        kind: "string".to_string(),
        value: Some(serde_json::Value::String(
          container_inspect
            .network_settings
            .unwrap()
            .networks
            .unwrap()["mediacloudai_global"]
            .ip_address
            .clone()
            .unwrap(),
        )),
        store: None,
        default: None,
      },
      Parameter {
        id: "ports".to_string(),
        kind: "array_of_string".to_string(),
        value: Some(generator.ports()),
        store: None,
        default: None,
      },
      Parameter {
        id: "instance_id".to_string(),
        kind: "string".to_string(),
        value: Some(serde_json::Value::String(container.id.to_string())),
        store: None,
        default: None,
      },
    ];

    let job_result = job_result.with_parameters(&mut output_parameters);

    Ok(job_result)
  }

  async fn delete(
    &self,
    parameters: &WorkerParameters,
    job_result: JobResult,
  ) -> Result<JobResult> {
    let docker = mcai_docker::Docker::connect_with_socket_defaults()
      .map_err(|e| Error::Docker(ConnectClient, e.to_string()))?;

    let worker_name = get_worker_name(parameters, job_result.get_job_id());

    let container = container::Container::get_by_name(&docker, &worker_name)
      .await
      .ok_or_else(|| {
        Error::Docker(
          GetContainer,
          format!("Unable to retrieve container {}", worker_name),
        )
      })?;

    container
      .stop(&docker, 0)
      .await
      .map_err(|e| Error::Docker(StopContainer, e.to_string()))?;
    info!("Worker {} stopped.", worker_name);

    container
      .remove(&docker, true)
      .await
      .map_err(|e| Error::Docker(RemoveContainer, e.to_string()))?;
    info!("Worker {} removed.", worker_name);

    Ok(job_result)
  }
}
