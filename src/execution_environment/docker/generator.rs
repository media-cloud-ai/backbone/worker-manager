use crate::{
  error::Result,
  execution_environment::{tools, Constraints},
  WorkerParameters,
};
use mcai_docker::DockerCredentials;
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use std::fs;

pub struct DockerGenerator {
  worker_name: String,
  image: String,
  ports: Vec<u64>,
  registry: Option<String>,
  registry_credentials: Option<RegistryCredentials>,
  envs: Option<Envs>,
  volumes: Option<Volumes>,
}

impl DockerGenerator {
  pub fn new(parameters: &WorkerParameters, job_id: u64) -> Result<Self> {
    let worker_name = tools::get_worker_name(parameters, job_id);
    let image = parameters.worker.name.clone();
    let registry = parameters.worker.registry.clone();
    let mut ports = parameters.ports.inputs.clone();

    ports.append(&mut parameters.ports.outputs.clone());

    let registry_credentials = Self::load("./config/docker/registries.yml")?;
    let envs = Self::load("./config/docker/env.yml")?;
    let volumes = Self::load("./config/docker/volumes.yml")?;

    Ok(Self {
      worker_name,
      image,
      ports,
      registry,
      registry_credentials,
      envs,
      volumes,
    })
  }

  pub fn worker_name(&self) -> String {
    self.worker_name.split('/').last().unwrap().to_string()
  }

  pub fn env(&self) -> Vec<(&str, &(dyn ToString + Send + Sync))> {
    self
      .envs
      .as_ref()
      .map(|envs| {
        envs
          .envs
          .iter()
          .filter(|&item| item.constraints.match_worker_name(&self.image))
          .map(|item| {
            let value: &(dyn ToString + Send + Sync) = &item.value;
            (item.name.as_str(), value)
          })
          .collect()
      })
      .unwrap_or_default()
  }

  pub fn ports(&self) -> serde_json::Value {
    let ports = self
      .ports
      .iter()
      .map(|port| serde_json::Value::String(port.to_string()))
      .collect();

    serde_json::Value::Array(ports)
  }

  pub fn port_bindings(&self) -> Vec<(u16, Option<u16>, Option<&str>)> {
    self
      .ports
      .iter()
      .map(|port| (*port as u16, Some(*port as u16), Some("127.0.0.1")))
      .collect()
  }

  pub fn volumes(&self) -> Vec<(&str, &str)> {
    self
      .volumes
      .as_ref()
      .map(|volumes| {
        volumes
          .volumes
          .iter()
          .filter_map(|item| {
            item
              .constraints
              .match_worker_name(&self.worker_name)
              .then_some((item.src.as_str(), item.dest.as_str()))
          })
          .collect()
      })
      .unwrap_or_default()
  }

  pub fn credentials(&self) -> DockerCredentials {
    self
      .registry_credentials
      .as_ref()
      .map(|registry_credentials| {
        let registry = registry_credentials
          .registries
          .iter()
          .find(|registry| self.is_registry(registry))
          .unwrap();

        DockerCredentials {
          password: Some(registry.registry_token.clone()),
          username: Some(registry.username.clone()),
          ..Default::default()
        }
      })
      .unwrap_or_default()
  }

  fn load<T: DeserializeOwned>(path: &str) -> Result<Option<T>> {
    if std::path::Path::new(path).exists() {
      let content: String = fs::read_to_string(path)?;
      Ok(serde_yaml::from_str(&content)?)
    } else {
      Ok(None)
    }
  }

  fn is_registry(&self, registry: &Registry) -> bool {
    self.registry.as_ref() == Some(&registry.name)
  }
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct RegistryCredentials {
  pub registries: Vec<Registry>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Registry {
  pub name: String,
  pub username: String,
  pub registry_token: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct Volumes {
  pub volumes: Vec<Volume>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct Volume {
  pub src: String,
  pub dest: String,
  #[serde(flatten)]
  pub constraints: Constraints,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct Envs {
  pub envs: Vec<Env>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct Env {
  pub name: String,
  pub value: String,
  #[serde(flatten)]
  pub constraints: Constraints,
}
