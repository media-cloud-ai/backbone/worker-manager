mod docker {
  mod execution_environment;
  mod generator;

  pub use execution_environment::DockerExecutionEnvironment;
}
mod kubernetes {
  mod execution_environment;
  mod generator;

  pub use execution_environment::KubernetesExecutionEnvironment;
  use generator::KubernetesGenerator;
}
mod tools;

use crate::{error::Result, Sidecar, WorkerParameters};
pub use docker::DockerExecutionEnvironment;
pub use kubernetes::KubernetesExecutionEnvironment;
use mcai_worker_sdk::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Constraints {
  #[serde(default)]
  only: Vec<String>,
}

impl Constraints {
  fn match_worker_name(&self, worker_name: &str) -> bool {
    self.only.is_empty() || self.only.contains(&worker_name.to_string())
  }

  fn match_sidecars_names(&self, sidecars: &[Sidecar]) -> bool {
    sidecars
      .iter()
      .map(|sidecar| self.only.contains(&sidecar.name))
      .any(|x| x)
  }
}

#[async_trait::async_trait]
pub trait ExecutionEnvironment {
  async fn create(&self, parameters: &WorkerParameters, job_result: JobResult)
    -> Result<JobResult>;
  async fn delete(&self, parameters: &WorkerParameters, job_result: JobResult)
    -> Result<JobResult>;
}
