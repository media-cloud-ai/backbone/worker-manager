use crate::{
  error::Result,
  execution_environment::{
    tools::{create_image_name, get_worker_name},
    Constraints,
  },
  Sidecar, WorkerParameters,
};
use k8s_openapi::{
  api::core::v1::{
    ConfigMapEnvSource, Container, ContainerPort, EnvFromSource, EnvVar, EnvVarSource,
    LocalObjectReference, ResourceRequirements, SecretEnvSource, Toleration, Volume, VolumeMount,
  },
  apimachinery::pkg::api::resource::Quantity,
};
use regex::Regex;
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use std::{
  collections::{BTreeMap, HashMap},
  fs,
  iter::FromIterator,
  vec,
};

use mcai_worker_sdk::prelude::info;

#[derive(Debug)]
pub struct KubernetesGenerator {
  worker_name: String,
  app_name: String,
  ports: Vec<u64>,
  registry: Option<String>,
  registry_credentials: Option<RegistryCredentials>,
  envs: Option<Envs>,
  volumes: Option<Volumes>,
  nodes: Option<Nodes>,
  tolerations: Option<Vec<Toleration>>,
  cpu_requests: Option<(usize, String)>,
  memory_requests: Option<(usize, String)>,
  resource_limit_scale: usize,
  sidecars: Vec<Sidecar>,
  labels: Option<MetadataLabels>,
  metadata_labels: Option<MetadataLabels>,
}

impl KubernetesGenerator {
  pub fn new(parameters: &WorkerParameters, job_id: u64) -> Result<Self> {
    let worker_name = parameters.worker.name.clone();
    let app_name = get_worker_name(parameters, job_id);
    let registry = parameters.worker.registry.clone();
    let mut ports = parameters.ports.inputs.clone();

    ports.append(&mut parameters.ports.outputs.clone());

    let registry_credentials = Self::load("./config/kubernetes/registries.yml")?;
    let envs = Self::load("./config/kubernetes/env.yml")?;
    let volumes = Self::load("./config/kubernetes/volumes.yml")?;
    let nodes = Self::load("./config/kubernetes/node_selectors.yml")?;
    let tolerations = Self::load("./config/kubernetes/tolerations.yml")?;
    let labels = Self::load("./config/kubernetes/labels.yml")?;
    let metadata_labels = Self::load("./config/kubernetes/metadata_labels.yml")?;

    info!("{:?}", worker_name);

    let cpu_requests = Self::parse_requests(&parameters.worker.cpu)?;
    let memory_requests = Self::parse_requests(&parameters.worker.ram)?;
    let resource_limit_scale = parameters.worker.scale.unwrap_or(1);

    Ok(Self {
      worker_name,
      app_name,
      ports,
      registry,
      registry_credentials,
      envs,
      volumes,
      nodes,
      tolerations,
      cpu_requests,
      memory_requests,
      resource_limit_scale,
      labels,
      metadata_labels,
      sidecars: parameters.worker.sidecars.clone().unwrap_or_default(),
    })
  }

  pub fn app_name(&self) -> String {
    self.app_name.clone()
  }

  pub fn image_pull_secrets(&self) -> Vec<LocalObjectReference> {
    self
      .registry_credentials
      .as_ref()
      .map(|registry_credentials| {
        registry_credentials
          .registries
          .iter()
          .filter(|&registry| self.is_registry(registry))
          .map(|registry| LocalObjectReference {
            name: Some(registry.secret.clone()),
          })
          .collect()
      })
      .unwrap_or_default()
  }

  pub fn ports(&self) -> Vec<ContainerPort> {
    self
      .ports
      .iter()
      .map(|port| ContainerPort {
        container_port: *port as i32,
        protocol: Some("UDP".to_string()),
        ..Default::default()
      })
      .collect()
  }

  pub fn env(&self) -> Vec<EnvVar> {
    self
      .envs
      .clone()
      .map(|envs| {
        envs
          .envs
          .into_iter()
          .filter_map(|item| {
            item
              .constraints
              .match_worker_name(&self.worker_name)
              .then_some(EnvVar {
                name: item.name,
                value: item.value,
                value_from: item.value_from,
              })
          })
          .collect()
      })
      .unwrap_or_default()
  }

  pub fn env_from(&self) -> Vec<EnvFromSource> {
    self
      .envs
      .clone()
      .map(|envs| {
        envs
          .envs_from
          .into_iter()
          .filter_map(|item| {
            item
              .constraints
              .match_worker_name(&self.worker_name)
              .then_some(EnvFromSource {
                config_map_ref: item.config_map_ref,
                prefix: item.prefix,
                secret_ref: item.secret_ref,
              })
          })
          .collect()
      })
      .unwrap_or_default()
  }

  pub fn volumes(&self) -> Vec<Volume> {
    self
      .volumes
      .as_ref()
      .map(|volumes| {
        volumes
          .volumes
          .iter()
          .filter(|item| {
            item.constraints.match_worker_name(&self.worker_name)
              || item.constraints.match_sidecars_names(&self.sidecars)
          })
          .cloned()
          .map(|item| item.volume)
          .collect()
      })
      .unwrap_or_default()
  }

  pub fn volume_mounts(&self) -> Vec<VolumeMount> {
    self
      .volumes
      .as_ref()
      .map(|volumes| {
        volumes
          .volumes
          .iter()
          .filter(|item| item.constraints.match_worker_name(&self.worker_name))
          .cloned()
          .map(|item| item.volume_mount)
          .collect()
      })
      .unwrap_or_default()
  }

  pub fn labels(&self) -> BTreeMap<String, String> {
    let mut labels = BTreeMap::from([("app".to_string(), self.app_name.clone())]);
    let labels_from_config = self
      .labels
      .as_ref()
      .map(|ml| BTreeMap::from_iter(ml.labels.iter().map(|(k, v)| (k.clone(), v.clone()))));
    labels.append(&mut labels_from_config.unwrap_or_default());

    labels
  }

  pub fn node_selector(&self) -> Option<BTreeMap<String, String>> {
    self.nodes.as_ref().map(|ns| {
      BTreeMap::from_iter(
        ns.node_selectors
          .iter()
          .map(|(k, v)| (k.clone(), v.clone())),
      )
    })
  }

  pub fn pod_tolerations(&self) -> Option<Vec<Toleration>> {
    self.tolerations.clone()
  }

  pub fn metadata_labels(&self) -> BTreeMap<String, String> {
    let mut metadata_labels = BTreeMap::from([("app".to_string(), self.app_name.clone())]);
    let metadata_labels_from_config = self
      .metadata_labels
      .as_ref()
      .map(|ml| BTreeMap::from_iter(ml.labels.iter().map(|(k, v)| (k.clone(), v.clone()))));
    metadata_labels.append(&mut metadata_labels_from_config.unwrap_or_default());

    metadata_labels
  }

  pub fn resource_requirements(&self) -> ResourceRequirements {
    let cpu_requested = self
      .cpu_requests
      .as_ref()
      .map(|(value, unit)| format!("{}{}", value, unit))
      .unwrap_or_default();
    let memory_requested = self
      .memory_requests
      .as_ref()
      .map(|(value, unit)| format!("{}{}", value, unit))
      .unwrap_or_default();

    let resource_requests = BTreeMap::from([
      ("cpu".to_string(), Quantity(cpu_requested)),
      ("memory".to_string(), Quantity(memory_requested)),
    ]);

    let cpu_limit = self
      .cpu_requests
      .as_ref()
      .map(|(value, unit)| format!("{}{}", value * self.resource_limit_scale, unit))
      .unwrap_or_default();
    let memory_limit = self
      .memory_requests
      .as_ref()
      .map(|(value, unit)| format!("{}{}", value * self.resource_limit_scale, unit))
      .unwrap_or_default();

    let resource_limits = BTreeMap::from([
      ("cpu".to_string(), Quantity(cpu_limit)),
      ("memory".to_string(), Quantity(memory_limit)),
    ]);

    ResourceRequirements {
      limits: Some(resource_limits),
      requests: Some(resource_requests),
    }
  }

  pub fn get_sidecars(&self) -> Vec<Container> {
    self
      .sidecars
      .iter()
      .map(|sidecar| {
        let generator = KubernetesGenerator {
          worker_name: sidecar.name.clone(),
          app_name: sidecar.name.clone(),
          ports: vec![],
          registry: sidecar.registry.clone(),
          registry_credentials: Self::load("./config/kubernetes/registries.yml").unwrap_or(None),
          envs: Self::load("./config/kubernetes/env.yml").unwrap_or(None),
          volumes: Self::load("./config/kubernetes/volumes.yml").unwrap_or(None),
          nodes: Self::load("./config/kubernetes/node_selectors.yml").unwrap_or(None),
          tolerations: Self::load("./config/kubernetes/tolerations.yml").unwrap_or(None),
          cpu_requests: Self::parse_requests(&sidecar.cpu)
            .unwrap_or_else(|_| Some((250, "m".to_string()))),
          memory_requests: Self::parse_requests(&sidecar.ram)
            .unwrap_or_else(|_| Some((250, "Mi".to_string()))),
          resource_limit_scale: sidecar.scale.unwrap_or(1),
          labels: Self::load("./config/kubernetes/labels.yml").unwrap_or(None),
          metadata_labels: Self::load("./config/kubernetes/metadata_labels.yml").unwrap_or(None),
          sidecars: vec![],
        };
        Container {
          name: sidecar.name.clone(),
          image: Some(create_image_name(
            sidecar.registry.clone(),
            sidecar.name.clone(),
            sidecar.version.clone(),
          )),
          image_pull_policy: Some("Always".to_string()),
          resources: Some(generator.resource_requirements()),
          env: Some(generator.env()),
          env_from: Some(generator.env_from()),
          volume_mounts: Some(generator.volume_mounts()),
          ..Default::default()
        }
      })
      .collect()
  }

  fn load<T: DeserializeOwned>(path: &str) -> Result<Option<T>> {
    if std::path::Path::new(path).exists() {
      let content: String = fs::read_to_string(path)?;
      Ok(serde_yaml::from_str(&content)?)
    } else {
      Ok(None)
    }
  }

  fn is_registry(&self, registry: &Registry) -> bool {
    self
      .sidecars
      .iter()
      .map(|sidecar| sidecar.registry.as_ref() == Some(&registry.name))
      .any(|x| x)
      || self.registry.as_ref() == Some(&registry.name)
  }

  fn parse_requests(limit: &Option<String>) -> Result<Option<(usize, String)>> {
    let re = Regex::new(r"([0-9]*)([a-zA-Z]*)")?;
    let result = limit
      .as_ref()
      .and_then(|limit| {
        re.captures(limit.as_str()).map(|captures| {
          captures
            .get(1)
            .map_or("", |m| m.as_str())
            .parse::<usize>()
            .map(|quantity| {
              let unit = captures.get(2).map_or("", |m| m.as_str());
              (quantity, unit.to_string())
            })
        })
      })
      .transpose()?;

    Ok(result)
  }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct RegistryCredentials {
  registries: Vec<Registry>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct Registry {
  name: String,
  secret: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct Envs {
  envs: Vec<Env>,
  envs_from: Vec<EnvSource>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct Env {
  name: String,
  value: Option<String>,
  value_from: Option<EnvVarSource>,
  #[serde(flatten)]
  constraints: Constraints,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct EnvSource {
  config_map_ref: Option<ConfigMapEnvSource>,
  prefix: Option<String>,
  secret_ref: Option<SecretEnvSource>,
  #[serde(flatten)]
  constraints: Constraints,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct Volumes {
  pub volumes: Vec<ProxyVolume>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct ProxyVolume {
  pub volume: Volume,
  pub volume_mount: VolumeMount,
  #[serde(flatten)]
  constraints: Constraints,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct Nodes {
  node_selectors: NodeSelectors,
}

pub type NodeSelectors = HashMap<String, String>;

#[derive(Clone, Debug, Deserialize, Serialize)]
struct MetadataLabels {
  labels: Labels,
}

pub type Labels = HashMap<String, String>;
