use super::KubernetesGenerator;
use crate::{
  error::{
    Error,
    KubernetesAction::{
      CreateClient, CreateDeployment, CreateGenerator, DeleteDeployment, ListPod,
    },
    Result,
  },
  execution_environment::{
    tools::{create_image_name, create_ports_array, get_worker_name},
    ExecutionEnvironment,
  },
  WorkerParameters,
};
use k8s_openapi::{
  api::{
    apps::v1::{Deployment, DeploymentSpec},
    core::v1::{Container, EnvVar, Pod, PodSpec, PodTemplateSpec},
  },
  apimachinery::pkg::apis::meta::v1::{LabelSelector, ObjectMeta},
};
use kube::api::{Api, DeleteParams, ListParams, PostParams, ResourceExt};
use mcai_worker_sdk::prelude::*;
use tokio::time::{sleep, Duration};

static TIMEOUT: i32 = 1200;

pub struct KubernetesExecutionEnvironment {}

#[async_trait::async_trait]
impl ExecutionEnvironment for KubernetesExecutionEnvironment {
  async fn create(
    &self,
    parameters: &WorkerParameters,
    job_result: JobResult,
  ) -> Result<JobResult> {
    let client = kube::Client::try_default()
      .await
      .map_err(|e| Error::Kubernetes(CreateClient, e.to_string()))?;

    let worker_deployment = from_parameters(parameters, job_result.get_job_id())?;

    let deployments: Api<Deployment> = Api::namespaced(
      client.clone(),
      &parameters
        .namespace
        .clone()
        .unwrap_or_else(|| "default".to_string()),
    );

    let post_params: PostParams = PostParams {
      dry_run: false,
      field_manager: Some("WorkerManager".to_string()),
    };

    deployments
      .create(&post_params, &worker_deployment)
      .await
      .map_err(|e| Error::Kubernetes(CreateDeployment, e.to_string()))?;
    info!("Deployment created.");

    let list_params = ListParams::default().labels(&format!(
      "app={}",
      worker_deployment.metadata.name.clone().unwrap_or_default()
    ));

    for deployment in deployments.list(&list_params).await.unwrap() {
      info!("Found deployment: {:?}", deployment.name());
    }

    let pods: Api<Pod> = Api::namespaced(
      client,
      &parameters
        .namespace
        .clone()
        .unwrap_or_else(|| "default".to_string()),
    );

    let mut readiness_flag = false;
    let mut readiness_iteration = 0;
    let mut worker_pod: Pod = Pod::default();
    info!("Pod checking");

    // Timeout after 10 minutes
    while !readiness_flag && readiness_iteration < TIMEOUT {
      if readiness_iteration % 20 == 0 {
        info!(
          "Checking.. (iteration {}, timeout {})",
          readiness_iteration, TIMEOUT
        );
      }

      for pod in pods
        .list(&list_params)
        .await
        .map_err(|e| Error::Kubernetes(ListPod, e.to_string()))?
      {
        if pod.status.as_ref().and_then(|status| status.phase.as_ref())
          == Some(&"Running".to_string())
        {
          info!("Pod running: {:?}", pod.name());
          readiness_flag = true;
          worker_pod = pod.clone();
        }
      }

      readiness_iteration += 1;
      sleep(Duration::from_millis(500)).await;
    }

    if !readiness_flag {
      error!("Cannot allocate pod, deleting deployment.");
      deployments
        .delete(
          &worker_deployment.name(),
          &DeleteParams {
            dry_run: false,
            grace_period_seconds: Some(0),
            preconditions: None,
            propagation_policy: None,
          },
        )
        .await
        .map_err(|e| Error::Kubernetes(DeleteDeployment, e.to_string()))?;

      info!("Deployment {} deleted.", worker_deployment.name());
      return Err(Error::Timeout("Cannot allocate pod.".to_string()));
    }

    let instance_id: String = worker_pod
      .clone()
      .status
      .unwrap()
      .container_statuses
      .unwrap()[0]
      .container_id
      .clone()
      .unwrap_or_default();

    let reduced_instance_id: &str = &instance_id.split("//").collect::<Vec<&str>>()[1][0..12];
    let ports_array = create_ports_array(parameters);

    debug!(
      "direct_messaging_queue_name: {:?}",
      parameters
        .direct_messaging_queue_name
        .clone()
        .unwrap_or_default()
    );
    let worker_pod_status = worker_pod.clone().status.unwrap();
    let host_ip = worker_pod_status.pod_ip.unwrap(); // should get internal pod ip and not node ip
    let first_pod_ip = worker_pod_status
      .pod_ips
      .unwrap()
      .first()
      .unwrap()
      .ip
      .clone()
      .unwrap_or_default();

    debug!("host_ip: {:?}", host_ip);
    debug!("pod_ips0: {:?}", first_pod_ip);
    debug!("container_id: {:?}", reduced_instance_id);

    let mut output_parameters = vec![
      Parameter {
        id: "direct_messaging_queue_name".to_string(),
        kind: "string".to_string(),
        value: Some(serde_json::Value::String(
          parameters.direct_messaging_queue_name.clone().unwrap(),
        )),
        store: None,
        default: None,
      },
      Parameter {
        id: "host_ip".to_string(),
        kind: "string".to_string(),
        value: Some(serde_json::Value::String(host_ip)),
        store: None,
        default: None,
      },
      Parameter {
        id: "groupe_ips".to_string(),
        kind: "string".to_string(),
        value: Some(serde_json::Value::String(first_pod_ip)),
        store: None,
        default: None,
      },
      Parameter {
        id: "ports".to_string(),
        kind: "array_of_string".to_string(),
        value: Some(serde_json::Value::Array(ports_array)),
        store: None,
        default: None,
      },
      Parameter {
        id: "instance_id".to_string(),
        kind: "string".to_string(),
        value: Some(serde_json::Value::String(reduced_instance_id.to_string())),
        store: None,
        default: None,
      },
    ];

    Ok(job_result.with_parameters(&mut output_parameters))
  }

  async fn delete(
    &self,
    parameters: &WorkerParameters,
    job_result: JobResult,
  ) -> Result<JobResult> {
    let client = kube::Client::try_default()
      .await
      .map_err(|e| Error::Kubernetes(CreateClient, e.to_string()))?;

    let deployments: Api<Deployment> = Api::namespaced(
      client.clone(),
      &parameters
        .namespace
        .clone()
        .unwrap_or_else(|| "default".to_string()),
    );

    let worker_deployment_name = get_worker_name(parameters, job_result.get_job_id());

    deployments
      .delete(
        &worker_deployment_name,
        &DeleteParams {
          dry_run: false,
          grace_period_seconds: Some(0),
          preconditions: None,
          propagation_policy: None,
        },
      )
      .await
      .map_err(|e| Error::Kubernetes(DeleteDeployment, e.to_string()))?;

    info!("Deployment {} deleted.", worker_deployment_name);

    Ok(job_result)
  }
}

fn from_parameters(parameters: &WorkerParameters, job_id: u64) -> Result<Deployment> {
  let generator = KubernetesGenerator::new(parameters, job_id)
    .map_err(|e| Error::Kubernetes(CreateGenerator, e.to_string()))?;
  info!("{:?}", generator);
  let mut containers = generator.get_sidecars();
  let mut container_env = generator.env();
  container_env.push(EnvVar {
    name: "AMQP_QUEUE".to_string(),
    value: Some(format!(
      "job_live_{}",
      parameters.worker.name.split('/').last().unwrap()
    )),
    value_from: None,
  });
  container_env.push(EnvVar {
    name: "DIRECT_MESSAGING_IDENTIFIER".to_string(),
    value: Some(
      parameters
        .direct_messaging_queue_name
        .clone()
        .unwrap_or_default(),
    ),
    value_from: None,
  });

  containers.push(Container {
    ports: Some(generator.ports()),
    name: generator.app_name(),
    image: Some(create_image_name(
      parameters.worker.registry.clone(),
      parameters.worker.name.clone(),
      parameters.worker.version.clone(),
    )),
    image_pull_policy: Some("Always".to_string()),
    resources: Some(generator.resource_requirements()),
    env: Some(container_env),
    env_from: Some(generator.env_from()),
    volume_mounts: Some(generator.volume_mounts()),
    ..Default::default()
  });

  Ok(Deployment {
    metadata: ObjectMeta {
      name: Some(generator.app_name()),
      namespace: parameters.namespace.clone(),
      labels: Some(generator.metadata_labels()),
      ..Default::default()
    },
    spec: Some(DeploymentSpec {
      replicas: Some(1),
      selector: LabelSelector {
        match_labels: Some(generator.labels()),
        ..Default::default()
      },
      template: PodTemplateSpec {
        metadata: Some(ObjectMeta {
          labels: Some(generator.metadata_labels()),
          ..Default::default()
        }),
        spec: Some(PodSpec {
          image_pull_secrets: Some(generator.image_pull_secrets()),
          node_selector: generator.node_selector(),
          containers,
          volumes: Some(generator.volumes()),
          tolerations: generator.pod_tolerations(),
          ..Default::default()
        }),
      },
      ..Default::default()
    }),
    ..Default::default()
  })
}
