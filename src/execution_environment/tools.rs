use crate::WorkerParameters;
use mcai_worker_sdk::prelude::*;
use regex::Regex;

pub fn create_ports_array(parameters: &WorkerParameters) -> Vec<serde_json::Value> {
  parameters
    .ports
    .inputs
    .iter()
    .chain(parameters.ports.outputs.iter())
    .map(|port| serde_json::Value::String(port.to_string()))
    .collect()
}

pub fn create_image_name(
  registry: Option<String>,
  name: String,
  version: Option<String>,
) -> String {
  let image_name = match registry.as_deref() {
    Some("dockerhub") | Some("") | None => name,
    Some(registry) => {
      warn!("Using custom registry");
      format!("{}/{}", registry, name)
    }
  };

  format!(
    "{}:{}",
    image_name,
    version.unwrap_or_else(|| "latest".to_string())
  )
}

pub fn get_worker_name(parameters: &WorkerParameters, job_id: u64) -> String {
  let worker_name = &parameters.worker.name;
  // App name
  let re = Regex::new(r"[a-z./]*/(.*)").unwrap();
  let replaced_image = worker_name.replace('_', "-");
  match re.captures(&replaced_image) {
    Some(caps) => format!("{}-{}", caps.get(1).map_or("", |m| m.as_str()), job_id),
    None => format!("{}-{}", replaced_image, job_id),
  }
}
