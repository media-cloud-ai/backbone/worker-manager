use crate::{
  execution_environment::{
    DockerExecutionEnvironment, ExecutionEnvironment, KubernetesExecutionEnvironment,
  },
  Action, ExecutionTarget, WorkerParameters,
};
use mcai_worker_sdk::prelude::*;

#[tokio::main]
pub async fn process(
  _channel: Option<McaiChannel>,
  parameters: WorkerParameters,
  job_result: JobResult,
) -> Result<JobResult> {
  debug!("Action: {:?}", parameters.action);
  debug!("{:?}", &parameters);

  let execution_environment: Box<dyn ExecutionEnvironment> = match &parameters.worker.exec {
    ExecutionTarget::Docker => Box::new(DockerExecutionEnvironment {}),
    ExecutionTarget::Kubernetes => Box::new(KubernetesExecutionEnvironment {}),
  };

  let job_result = match &parameters.action {
    Action::Create => execution_environment
      .create(&parameters, job_result.clone())
      .await
      .map_err(|error| {
        MessageError::ProcessingError(
          job_result
            .clone()
            .with_status(JobStatus::Error)
            .with_message(&format!("{}.", error)),
        )
      })?,
    Action::Delete => execution_environment
      .delete(&parameters, job_result.clone())
      .await
      .map_err(|error| {
        MessageError::ProcessingError(
          job_result
            .clone()
            .with_status(JobStatus::Error)
            .with_message(&format!("{}.", error)),
        )
      })?
      .with_status(JobStatus::Completed),
  };

  Ok(job_result)
}
