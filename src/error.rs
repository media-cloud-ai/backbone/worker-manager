use std::{
  fmt::{Display, Formatter},
  num::ParseIntError,
};

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum KubernetesAction {
  CreateClient,
  CreateGenerator,
  CreateDeployment,
  ListPod,
  DeleteDeployment,
}

#[derive(Debug)]
pub enum DockerAction {
  ConnectClient,
  CreateContainer,
  GetContainer,
  InspectContainer,
  PullImage,
  RemoveContainer,
  RunContainer,
  StopContainer,
}

#[derive(Debug)]
pub enum Error {
  ConfigurationParsing(Box<Error>),
  Docker(DockerAction, String),
  Io(std::io::Error),
  Kubernetes(KubernetesAction, String),
  ParseInt(ParseIntError),
  Regex(regex::Error),
  SerdeJson(serde_json::Error),
  SerdeYaml(serde_yaml::Error),
  Timeout(String),
}

impl From<std::io::Error> for Error {
  fn from(error: std::io::Error) -> Self {
    Error::Io(error)
  }
}

impl From<ParseIntError> for Error {
  fn from(error: ParseIntError) -> Self {
    Error::ParseInt(error)
  }
}

impl From<regex::Error> for Error {
  fn from(error: regex::Error) -> Self {
    Error::Regex(error)
  }
}

impl From<serde_json::Error> for Error {
  fn from(error: serde_json::Error) -> Self {
    Error::SerdeJson(error)
  }
}

impl From<serde_yaml::Error> for Error {
  fn from(error: serde_yaml::Error) -> Self {
    Error::SerdeYaml(error)
  }
}

impl From<String> for Error {
  fn from(error: String) -> Self {
    Error::Timeout(error)
  }
}

impl Display for Error {
  fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
    let error_message = match self {
      Error::ConfigurationParsing(error) => format!("Configuration parsing error: {}", error),
      Error::Kubernetes(action, error) => {
        format!("Kubernetes error on action {:?}. Error: {}", action, error)
      }
      Error::Docker(action, error) => {
        format!("Docker error on action: {:?}. Error: {}", action, error)
      }
      Error::Io(error) => format!("IO: {}", error),
      Error::ParseInt(error) => format!("Parse int: {}", error),
      Error::Regex(error) => format!("Regex: {}", error),
      Error::SerdeJson(error) => format!("Serde Json: {}", error),
      Error::SerdeYaml(error) => format!("Serde Yaml: {}", error),
      Error::Timeout(error) => format!("Timeout: {}", error),
    };

    f.write_str(&error_message)
  }
}
