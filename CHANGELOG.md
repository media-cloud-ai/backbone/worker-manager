<a name="unreleased"></a>
## [Unreleased]

### Feat
- format return message to stepflow
- enhance ci with cd
- add makefile
- add ci
- add dockerfile
- sdk wrapper around bash code
- modify local deployment file name

### Fix
- remove use of getop

### Test
- update examples


<a name="0.1.0"></a>
## 0.1.0 - 2020-11-26
### Feat
- add example messages
- add delete all pods
- use kubectl wait to wait for running pod
- first version of project

### Fix
- modify input and output message to match stepflow messages


[Unreleased]: https://gitlab.com/mediacloudai/pocs/worker-manager/compare/0.1.0...HEAD
